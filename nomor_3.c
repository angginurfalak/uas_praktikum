#include <stdio.h>

// Nama : Anggi nur palak
// Nim  : 302230006
// Tanggal: 07/February/2024
// 17.16

int main() {
    // Deklarasi variabel
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    // Loop untuk beberapa mahasiswa (diasumsikan 3 mahasiswa sebagai contoh)
    for (int mahasiswa = 1; mahasiswa <= 3; ++mahasiswa) {
        // Input nilai untuk setiap mahasiswa
        printf("Input nilai untuk Mahasiswa %d:\n", mahasiswa);
        printf("Quiz: "); scanf("%lf", &quiz);
        printf("Absen: "); scanf("%lf", &absen);
        printf("UTS: "); scanf("%lf", &uts);
        printf("UAS: "); scanf("%lf", &uas);
        printf("Tugas: "); scanf("%lf", &tugas);

        // Menampilkan nilai yang dimasukkan
        printf("Nilai untuk Mahasiswa %d:\n", mahasiswa);
        printf("Absen = %.2f UTS = %.2f\n", absen, uts);
        printf("Tugas = %.2f UAS = %.2f\n", tugas, uas);
        printf("Quiz = %.2f\n", quiz);

        // Menghitung total nilai
       nilai = (0.1 * absen) + (0.2 * tugas) + (0.4 * uts) + (0.5 * uas);

        // Penugasan nilai huruf berdasarkan total nilai
        if (nilai > 85 && nilai <= 100)
             Huruf_Mutu = 'A';
        else if (nilai > 70 && nilai <= 85)
             Huruf_Mutu = 'B';
        else if (nilai > 55 && nilai <= 70)
             Huruf_Mutu = 'C';
        else if (nilai > 40 && nilai <= 55)
             Huruf_Mutu = 'D';
        else if (nilai >= 0 && nilai <= 40)
             Huruf_Mutu = 'E';

        // Menampilkan nilai huruf yang diberikan
        printf("Huruf Mutu untuk Mahasiswa %d: %c\n\n", mahasiswa, Huruf_Mutu);
    }

    return 0;
}
